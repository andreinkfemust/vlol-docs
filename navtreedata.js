var NAVTREE =
[
  [ "VLOL", "index.html", [
    [ "VLOL - Visual Lidar Odometry Library", "index.html", null ],
    [ "lidar_odometry_lib", "md__home_andrzej_code_workspace_visual_lidar_odometry_src_lib_lidar_odometry_lib__r_e_a_d_m_e.html", null ],
    [ "visual_lidar_odometry_lib", "md__home_andrzej_code_workspace_visual_lidar_odometry_src_lib_visual_lidar_odometry_lib__r_e_a_d_m_e.html", null ],
    [ "visual_odometry_lib", "md__home_andrzej_code_workspace_visual_lidar_odometry_src_lib_visual_odometry_lib__r_e_a_d_m_e.html", null ],
    [ "displayer", "md__home_andrzej_code_workspace_visual_lidar_odometry_src_utils_displayer__r_e_a_d_m_e.html", null ],
    [ "open3d_conversions", "md__home_andrzej_code_workspace_visual_lidar_odometry_src_utils_open3d_utils_open3d_conversions__r_e_a_d_m_e.html", null ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", "namespacemembers_dup" ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_camera_8cpp.html",
"classrv_1_1_transform.html#afad80bbdb3366df2b79b25141ebecf68",
"common__vlo_8hpp.html#a2496e74f582d62d06ab2936a2366272a",
"geometry_8h.html#a32434c740b486ad3012394f6b0a535a2",
"string__utils_8cpp.html#a6314d0fea51137e774d7ce53faf98653"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';