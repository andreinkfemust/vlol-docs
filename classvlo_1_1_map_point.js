var classvlo_1_1_map_point =
[
    [ "Ptr", "classvlo_1_1_map_point.html#a46e96ca7064fb6a0e3b15f695ef1ec5f", null ],
    [ "MapPoint", "classvlo_1_1_map_point.html#ac45f924e221106f0e06a54f31deb3c9f", null ],
    [ "getColor", "classvlo_1_1_map_point.html#a009f8b69623b455d67dcf323e4a55868", null ],
    [ "getID", "classvlo_1_1_map_point.html#a08c2a00042d672c71e3e8122bd2bac2b", null ],
    [ "getPosition", "classvlo_1_1_map_point.html#a47e0642578c72744196dcc3c2719d931", null ],
    [ "setPosition", "classvlo_1_1_map_point.html#accd37a6b650bd91c411b8e6464640caa", null ],
    [ "color_", "classvlo_1_1_map_point.html#a43bc9e542c103b8808cacfad04ab3afe", null ],
    [ "descriptor_", "classvlo_1_1_map_point.html#ac43b776fa31b3ce68be87068f48ae9ad", null ],
    [ "id_", "classvlo_1_1_map_point.html#a2848158db943f04f6551ec179c8d93d7", null ],
    [ "position_", "classvlo_1_1_map_point.html#a47e8df29ba08389c3e7c96600168557e", null ],
    [ "viewing_direction_", "classvlo_1_1_map_point.html#a8ca6d166cb191c4708742c2c6d23a007", null ],
    [ "visible_times_", "classvlo_1_1_map_point.html#aa811f6a8425e1cb3a7b5fd2731c3a3ca", null ]
];