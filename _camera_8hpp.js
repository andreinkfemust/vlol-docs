var _camera_8hpp =
[
    [ "Camera", "classvlo_1_1_camera.html", "classvlo_1_1_camera" ],
    [ "getRGBvalueFromImageNormalized", "_camera_8hpp.html#a5898555d984b9ec5bd5adfc9601fac32", null ],
    [ "isInImage", "_camera_8hpp.html#a2c04ee36f9c383b8c867f9167146decf", null ],
    [ "projectPointsToImagePlane", "_camera_8hpp.html#aea3a309275141f8316c313245c3fa518", null ],
    [ "projectPointToImagePlane", "_camera_8hpp.html#aab99ac59ce412c1b1c8d96dbb0092f7c", null ],
    [ "projectPointToImagePlane", "_camera_8hpp.html#a026a02fa33d14e1383066277a8a31d52", null ],
    [ "world2camera", "_camera_8hpp.html#ac3c8e7b858d0badd0f0c75604e0b140c", null ]
];