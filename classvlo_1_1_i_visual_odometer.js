var classvlo_1_1_i_visual_odometer =
[
    [ "Ptr", "classvlo_1_1_i_visual_odometer.html#a5c416398128f58d367e9c1dc9ddb7d74", null ],
    [ "IVisualOdometer", "classvlo_1_1_i_visual_odometer.html#a1cbbb2aecec1188e15bcb1283e292b83", null ],
    [ "~IVisualOdometer", "classvlo_1_1_i_visual_odometer.html#a655ae76efc14cbfd1c13e2f1c3eb49d8", null ],
    [ "getFramesHistory", "classvlo_1_1_i_visual_odometer.html#af1bc73b01d10313724c867aa90ce4e25", null ],
    [ "getFrameToFrameTransformation", "classvlo_1_1_i_visual_odometer.html#a614b6b2114e43057042d80f17e82f171", null ],
    [ "getLastPose", "classvlo_1_1_i_visual_odometer.html#a43e837cdd8275bf5b8f41a02cc1c4475", null ],
    [ "processFrame", "classvlo_1_1_i_visual_odometer.html#aa0819c50f797c6169990dadf939d68e4", null ],
    [ "pushFrameToHistory", "classvlo_1_1_i_visual_odometer.html#a4ff7efbaacf6eb7f43adb9d19e9801dd", null ],
    [ "current_frame_", "classvlo_1_1_i_visual_odometer.html#ab55bb93bb1f166603af0d3ed0d930096", null ],
    [ "debug_", "classvlo_1_1_i_visual_odometer.html#a84933dcb988e559518e7f7c3210a16f8", null ],
    [ "history_frames_", "classvlo_1_1_i_visual_odometer.html#a16791c708f394fab262aeb08b5df0cc7", null ],
    [ "initial_pose_", "classvlo_1_1_i_visual_odometer.html#a6360f1d8013e3ed1a7120a2fcaa9d99d", null ],
    [ "reference_frame_", "classvlo_1_1_i_visual_odometer.html#a005c9ba0d915cbc619a635835f2de5e3", null ],
    [ "scale_", "classvlo_1_1_i_visual_odometer.html#a86adc0b0ea7774e69ef77b8de3aaf01a", null ],
    [ "verbose_", "classvlo_1_1_i_visual_odometer.html#a51f614d0d5f7c5c9c0a0b55e33ff08ee", null ],
    [ "vo_state_", "classvlo_1_1_i_visual_odometer.html#ac08b2757d4e33aa9d86d8a642b2f0b04", null ]
];