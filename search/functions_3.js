var searchData=
[
  ['degrees',['Degrees',['../namespacerv.html#a547196d9a0011ffa8d8c79fbb9a96589',1,'rv']]],
  ['description',['description',['../struct_point_matcher_support_1_1_r_o_s_logger.html#a1f193af597f56ca8c2840f0e0fe97173',1,'PointMatcherSupport::ROSLogger']]],
  ['descriptorexists',['descriptorExists',['../class_point_matcher__ros_1_1_stamped_point_cloud.html#a2c8ac55a686878d890c04e3735909425',1,'PointMatcher_ros::StampedPointCloud']]],
  ['directionhinticp',['directionHintICP',['../classvlo_1_1_lidar_odometer1_d_o_f.html#a6072f177a09ba2ff71ca0edbbbb8cd04',1,'vlo::LidarOdometer1DOF']]],
  ['distance',['Distance',['../namespacerv.html#ac31bc6b07a06f3504a967b655112bfe1',1,'rv']]],
  ['distancesquared',['DistanceSquared',['../namespacerv.html#a0087a7c4095fd2387945b1c858e6dde0',1,'rv']]],
  ['do_5ficp_5f1d',['do_icp_1d',['../classvlo_1_1_lidar_odometer1_d_o_f.html#afdc8dae8f76e5cab26e5515236a46e9e',1,'vlo::LidarOdometer1DOF']]],
  ['dot',['Dot',['../namespacerv.html#a447ff74d43f6bb157b89cd220d5a25a8',1,'rv::Dot(const Vector3f &amp;v1, const Vector3f &amp;v2)'],['../namespacerv.html#ad857ea119cd90de28a1f651c2ad1cdd7',1,'rv::Dot(const Normal3f &amp;n1, const Vector3f &amp;v2)'],['../namespacerv.html#a0a4f65e8dcae35af8d113e03ad52c174',1,'rv::Dot(const Vector3f &amp;v1, const Normal3f &amp;n2)'],['../namespacerv.html#a9688fc4d0425f0b037e9f30312f0a145',1,'rv::Dot(const Normal3f &amp;n1, const Normal3f &amp;n2)']]],
  ['drawgoodandbadkeypoints',['drawGoodAndBadKeypoints',['../namespacevlo.html#a2eb29f694374e7dcc1dcfd6c9536e8c3',1,'vlo']]],
  ['drawgoodkeypoints',['drawGoodKeyPoints',['../namespacevlo.html#ac02ae7060c6f11b8861e5da8e7f480c3',1,'vlo']]],
  ['drawkeypoints',['drawKeypoints',['../namespacevlo.html#a451510ae70cf6b5d130b4c5e271c50aa',1,'vlo']]],
  ['drawmap',['drawMap',['../namespacevlo.html#a413ead6e2c301a0d1ead2b40de34583d',1,'vlo']]],
  ['drawmatchesimagetomap',['drawMatchesImageToMap',['../namespacevlo.html#a6e4d357ab2e254a3e94aa520964ab64e',1,'vlo::drawMatchesImageToMap(const IFrame::Ptr &amp;frame1, const Points3Dd &amp;points3d, const IFrame::Ptr &amp;frame2, const ImagePointsd &amp;image_points, const std::vector&lt; cv::DMatch &gt; &amp;matches, const std::string &amp;window_name)'],['../namespacevlo.html#a700071ef39f92da9f8770522d49a46dc',1,'vlo::drawMatchesImageToMap(const IFrame::Ptr &amp;frame1, const ImagePointsd &amp;image_points, const IFrame::Ptr &amp;frame2, const Points3Dd &amp;points3d, const std::vector&lt; cv::DMatch &gt; &amp;matches, const std::string &amp;window_name)']]],
  ['drawpoints',['drawPoints',['../namespacevlo.html#a25e79b3ff52a37fd29eed8fe16461b60',1,'vlo']]],
  ['drawprojectedpoints',['drawProjectedPoints',['../namespacevlo.html#a111b209e980ebdb395fab41ecdbcaaad',1,'vlo']]]
];
