var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuvwxyz~",
  1: "acefhiklmnprstv",
  2: "koprtv",
  3: "cfghiklmoprstuv",
  4: "abcdefghiklmnoprstuvwxyz~",
  5: "_acdefghiklmnoprstv",
  6: "cdgikmnptv",
  7: "v",
  8: "ilr",
  9: "befiklopt",
  10: "bnpst",
  11: "dlov"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Pages"
};

