var hierarchy =
[
    [ "rv::traits::access< PointT, D >", "structrv_1_1traits_1_1access.html", null ],
    [ "rv::traits::access< PointT, 0 >", "structrv_1_1traits_1_1access_3_01_point_t_00_010_01_4.html", null ],
    [ "rv::traits::access< PointT, 1 >", "structrv_1_1traits_1_1access_3_01_point_t_00_011_01_4.html", null ],
    [ "rv::traits::access< PointT, 2 >", "structrv_1_1traits_1_1access_3_01_point_t_00_012_01_4.html", null ],
    [ "rv::traits::access< rv::Normal3f, 0 >", "structrv_1_1traits_1_1access_3_01rv_1_1_normal3f_00_010_01_4.html", null ],
    [ "rv::traits::access< rv::Normal3f, 1 >", "structrv_1_1traits_1_1access_3_01rv_1_1_normal3f_00_011_01_4.html", null ],
    [ "rv::traits::access< rv::Normal3f, 2 >", "structrv_1_1traits_1_1access_3_01rv_1_1_normal3f_00_012_01_4.html", null ],
    [ "rv::traits::access< rv::Point3f, 0 >", "structrv_1_1traits_1_1access_3_01rv_1_1_point3f_00_010_01_4.html", null ],
    [ "rv::traits::access< rv::Point3f, 1 >", "structrv_1_1traits_1_1access_3_01rv_1_1_point3f_00_011_01_4.html", null ],
    [ "rv::traits::access< rv::Point3f, 2 >", "structrv_1_1traits_1_1access_3_01rv_1_1_point3f_00_012_01_4.html", null ],
    [ "rv::traits::access< rv::Vector3f, 0 >", "structrv_1_1traits_1_1access_3_01rv_1_1_vector3f_00_010_01_4.html", null ],
    [ "rv::traits::access< rv::Vector3f, 1 >", "structrv_1_1traits_1_1access_3_01rv_1_1_vector3f_00_011_01_4.html", null ],
    [ "rv::traits::access< rv::Vector3f, 2 >", "structrv_1_1traits_1_1access_3_01rv_1_1_vector3f_00_012_01_4.html", null ],
    [ "ConfigReader", "struct_config_reader.html", null ],
    [ "KITTI::Odometry::errors", "struct_k_i_t_t_i_1_1_odometry_1_1errors.html", null ],
    [ "vlo::FeatureMatcher", "classvlo_1_1_feature_matcher.html", null ],
    [ "vlo::IFeatureDetector", "classvlo_1_1_i_feature_detector.html", [
      [ "vlo::FeatureDetector", "classvlo_1_1_feature_detector.html", null ]
    ] ],
    [ "vlo::IFrame", "classvlo_1_1_i_frame.html", [
      [ "vlo::LidarFrame", "classvlo_1_1_lidar_frame.html", null ],
      [ "vlo::VisualFrame", "classvlo_1_1_visual_frame.html", null ],
      [ "vlo::VisualLidarFrame", "classvlo_1_1_visual_lidar_frame.html", null ],
      [ "vlo::VisualLidarFusionFrame", "classvlo_1_1_visual_lidar_fusion_frame.html", null ]
    ] ],
    [ "vlo::IOdometer", "classvlo_1_1_i_odometer.html", [
      [ "vlo::ComboVLO", "classvlo_1_1_combo_v_l_o.html", null ],
      [ "vlo::FeatureFusionVLO", "classvlo_1_1_feature_fusion_v_l_o.html", null ],
      [ "vlo::HuangVLO", "classvlo_1_1_huang_v_l_o.html", null ],
      [ "vlo::ILidarOdometer", "classvlo_1_1_i_lidar_odometer.html", [
        [ "vlo::LidarOdometerFrameToFrame", "classvlo_1_1_lidar_odometer_frame_to_frame.html", null ],
        [ "vlo::LO_FrameToFrame_ImageBasedPlaneSegmentation", "classvlo_1_1_l_o___frame_to_frame___image_based_plane_segmentation.html", null ],
        [ "vlo::LO_FrameToFrame_PlaneSegmentation", "classvlo_1_1_l_o___frame_to_frame___plane_segmentation.html", null ]
      ] ],
      [ "vlo::IVisualOdometer", "classvlo_1_1_i_visual_odometer.html", [
        [ "vlo::VO_FrameToFrame_5point", "classvlo_1_1_v_o___frame_to_frame__5point.html", null ]
      ] ],
      [ "vlo::VLO6dof", "classvlo_1_1_v_l_o6dof.html", null ]
    ] ],
    [ "vlo::ISensor", "classvlo_1_1_i_sensor.html", [
      [ "vlo::Camera", "classvlo_1_1_camera.html", null ]
    ] ],
    [ "KITTICalibration", "class_k_i_t_t_i_calibration.html", null ],
    [ "rv::Laserscan", "classrv_1_1_laserscan.html", null ],
    [ "vlo::LidarOdometer1DOF", "classvlo_1_1_lidar_odometer1_d_o_f.html", null ],
    [ "vlo::LineDetector", "classvlo_1_1_line_detector.html", null ],
    [ "Logger", null, [
      [ "PointMatcherSupport::ROSLogger", "struct_point_matcher_support_1_1_r_o_s_logger.html", null ]
    ] ],
    [ "vlo::Map", "classvlo_1_1_map.html", null ],
    [ "vlo::MapPoint", "classvlo_1_1_map_point.html", null ],
    [ "rv::Normal3f", "structrv_1_1_normal3f.html", null ],
    [ "PointMatcher_ros::PmTf", "class_point_matcher__ros_1_1_pm_tf.html", null ],
    [ "rv::Point3f", "structrv_1_1_point3f.html", null ],
    [ "PointMatcher_ros::PointMatcherFilterInterface", "class_point_matcher__ros_1_1_point_matcher_filter_interface.html", null ],
    [ "rv::Ray", "structrv_1_1_ray.html", null ],
    [ "RvizBridgeViewer", "class_rviz_bridge_viewer.html", null ],
    [ "vlo::SanityChecker", "classvlo_1_1_sanity_checker.html", null ],
    [ "PointMatcher_ros::StampedPointCloud", "class_point_matcher__ros_1_1_stamped_point_cloud.html", null ],
    [ "rv::Transform", "classrv_1_1_transform.html", null ],
    [ "rv::Vector3f", "structrv_1_1_vector3f.html", null ]
];